const admin = require("firebase-admin");
const serviceAccount = require("./firebasekey/udru-navigator-firebase-adminsdk-s30uj-9815d243f0.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://udru-navigator.firebaseio.com"
});

const db = admin.database();
const ref = db.ref("/");

const initFirebase = () => {
    return new Promise((resolve, reject) => {
        return ref.on("value", function(snapshot) {
            if (snapshot.val()) {
                resolve(snapshot.val());
            } else {
                reject({ "error": 404, "message": "data ot found" })
            }
        }, function(errorObject) {
            console.log("The read failed: " + errorObject.code);
            reject({ "error": 500, "message": errorObject })
        });
    })

};

module.exports = { initFirebase };