const express = require('express');
const { initFirebase } = require('./firebase.js');
const app = express()
const PORT = 3000;
let provinceList = {};

app.use((req, res, next) => {
    initFirebase().then(resp => {
        provinceList = resp;
        next();
    }).catch(err => {
        console.log('firebase error', err);
    })
})

app.use('/', express.static('./client/public'))

app.get('/getprovinces', function(req, res) {
    res.send(provinceList)
})

app.listen(PORT, '0.0.0.0');
console.log('serverlisten http://localhost:' + PORT);