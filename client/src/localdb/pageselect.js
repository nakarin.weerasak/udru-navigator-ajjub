import { writable } from 'svelte/store';

const defaultPage = "home"
export const pageSelect = writable(defaultPage);