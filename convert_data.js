const JQL = require('jqljs');
const fs = require('fs');
const provinceDB = require('./db.json')
    // create query
const str = ['บึงกาฬ', 'หนองคาย', 'สกลนคร', 'ขอนแก่น'];

let resp = {};
str.forEach(pr => {
    const provinceMath = provinceDB.filter(data => data.province == pr);
    let provinceGroup = {};
    provinceMath.forEach((obj) => {
        provinceGroup[obj['amphoe']] = {}
    });
    provinceMath.forEach((obj) => {
        provinceGroup[obj['amphoe']][obj['district']] = { "หมู่บ้าน": { lat: 0.0, lng: 0.0 } };
    })
    Object.assign(resp, {
        [pr]: provinceGroup
    });
})

fs.writeFileSync('./resp.json', JSON.stringify(resp));